import React from 'react';
import { Button } from './styles';


const Task = ({ details, onDelete }) => (
    <li>
        {details.name} 
        <Button onClick={() => onDelete(details.id)}>X</Button>
    </li>
);


export default Task;