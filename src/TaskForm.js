import React, {Component} from 'react';
import { Button } from './styles';

class TaskForm extends Component {
  state = {
    newTask: ''
  }

  handleChange = (event) => {
    this.setState({ newTask: event.currentTarget.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const entry = {
      id: new Date().getTime(),
      name: this.state.newTask
    };
    this.props.onAddTask(entry);

    this.setState({newTask: ""})
  };

  render() {
    return(
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          value={this.state.newTask}
          onChange={this.handleChange}
          placeholder="Nouvelle chose à faire"/>
        <Button comwatt white>Ajouter</Button>
      </form>
    )
  }
}

export default TaskForm;
