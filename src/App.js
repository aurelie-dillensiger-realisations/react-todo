import React from 'react';
import './App.css';
import Task from './Task';
import TaskForm from './TaskForm';
import { Header } from './styles';


class App extends React.Component {

  state = {
    tasks: [
      {id: 1, name: "Regarder les plantes pousser"},
      {id: 2, name: "Refaire le toit"},
      {id: 3, name: "Faire la caisse du chat"},
    ]
  };

  handleDelete = id => {
    const tasks = [...this.state.tasks];
    const index = tasks.findIndex(task => task.id === id);

    tasks.splice(index, 1);

    this.setState({ tasks });
  };

  handleAddTask = newTask => {
    const tasks = [...this.state.tasks];
    tasks.push(newTask);

    this.setState({ tasks });
  }

  render () {
    const header = {
      title: "Liste des choses à faire !",
      subtitle: "Car c'est très important de faire des choses !"
    };

    return (
      <div>
        <Header>
          <h1>{header.title}</h1>
          <h3>{header.subtitle}</h3>
        </Header>
        <ul>
          {this.state.tasks.map(task => (
            <Task details={task} onDelete={this.handleDelete} />
          ))}
        </ul>
            <TaskForm onAddTask={this.handleAddTask} />
      </div>
    );
  }
}

export default App;
