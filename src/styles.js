import styled from '@emotion/styled';


const Button = styled.button`
  color: ${props =>
    props.white ? 'white' : 'black'};
  background-color: ${props =>
    props.comwatt ? '#022c6b' : 'lightgrey'};
  border-radius: 4px;
  margin: 10px;
`

const Header = styled.header`
    background-color: #022c6b;
    padding: 20px;
    color: white;
`


export { Button, Header };
